Source: blends
Maintainer: Debian Pure Blend Team <debian-blends@lists.debian.org>
Uploaders: Petter Reinholdtsen <pere@debian.org>,
           Andreas Tille <tille@debian.org>,
           Jonas Smedegaard <dr@jones.dk>,
           Ole Streicher <olebole@debian.org>,
           Mike Gabriel <sunweaver@debian.org>
Section: devel
Priority: optional
Build-Depends: debhelper (>= 11~)
Build-Depends-Indep: dblatex,
                     dh-python,
                     python3-all,
                     python3-apt,
                     python3-debian,
                     python3-pytest,
                     python3-sphinx,
                     w3m,
                     xmlto
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/blends-team/blends
Vcs-Git: https://salsa.debian.org/blends-team/blends.git

Package: blends-dev
Architecture: all
Depends: debconf,
         debhelper (>= 9),
         make | build-essential,
         python3,
         python3-apt,
         python3-blends,
         bc,
         ${misc:Depends}
Suggests: blends-doc
Recommends: python3-psycopg2
Description: Debian Pure Blends common files for developing metapackages
 This package makes life easier when packaging metapackages.  Perhaps
 this will also encourage other people to build metapackages if there are
 easy to use templates where only the packages, the metapackage is depending
 from, to insert into the right place.

Package: blends-common
Architecture: all
Section: misc
Depends: adduser,
         debconf,
         menu,
         ${misc:Depends}
Suggests: blends-doc
Description: Debian Pure Blends common package
 This package builds the basic infra structure for metapackages.
 .
 This package provides some files which are common to metapackages
 of Common Debian Distributions. It introduces a method to handle
 system users in a group named according to the name of the
 Debian Pure Blend.

Package: blends-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: postscript-viewer,
          www-browser
Description: Debian Pure Blends documentation
 This paper is intended to people who are interested in the philosophy
 of Debian Pure Blends and the technique which is used to
 manage those projects.
 .
 It is explained in detail why these are no forks from Debian but reside
 completely inside the Debian GNU distribution and which
 advantages can be gathered by this approach.  The concept of
 metapackages and user role based menus is explained.  In short: This
 document describes why Debian Pure Blends are important to
 the vitality and quality of Debian.

Package: blends-tasks
Architecture: all
Section: misc
Priority: important
Depends: tasksel,
         ${misc:Depends}
Description: Debian Pure Blends tasks for new installations
 This package installs a choice of a default installation for each
 Debian Pure Blend when run from the Debian installer. The
 installation includes the tasks package of the blend, so a subsequent
 invocation of tasksel enables the choice of individual tasks.
 .
 The package is intended to be installed in the base system. Later
 (un)installation is harmless, but has no effect.

Package: python3-blends
Architecture: all
Section: python
Depends: python3-debian,
         ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends}
Suggests: python3-psycopg2,
          python3-apt
Description: Python 3 module for Debian Pure Blends support
 This package installs a module to handle Debian Pure Blends tasks.
 It reads the tasks description from unpacked Blend Metapackages
 sources. It is directly possible to create the debian/control file
 and the tasks definition files from it.
